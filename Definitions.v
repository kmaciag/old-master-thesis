Set Implicit Arguments.
Require Import TLC.LibLN.
Require Import Arith.
Require Import TLC.LibEnv.
Require Import TLC.LibTactics.
Require Import TLC.LibVar.
Require Import TLC.LibLN.

Inductive pre_type : Set :=
  | pre_type_bvar  : nat -> pre_type
  | pre_type_fvar  : var -> pre_type
  | pre_type_arrow : pre_type -> pre_type -> pre_type
  | pre_type_all   : pre_type -> pre_type.

Inductive pre_term : Set :=
  | pre_term_bvar : nat -> pre_term
  | pre_term_fvar : var -> pre_term
  | pre_term_abs  : pre_type -> pre_term -> pre_term
  | pre_term_app  : pre_term -> pre_term -> pre_term
  | pre_term_tabs : pre_term -> pre_term
  | pre_term_tapp : pre_term -> pre_type -> pre_term.

Fixpoint open_tt_rec (K : nat) (U : pre_type) (T : pre_type) : pre_type :=
  match T with
  | pre_type_bvar J      => If J = K then U else (pre_type_bvar J)
  | pre_type_fvar X      => pre_type_fvar X
  | pre_type_arrow T1 T2 => pre_type_arrow (open_tt_rec K U T1) (open_tt_rec K U T2)
  | pre_type_all T1   => pre_type_all (open_tt_rec (S K) U T1)
  end.

Definition open_tt T U := open_tt_rec 0 U T.

Fixpoint open_te_rec (K : nat) (U : pre_type) (e : pre_term) : pre_term :=
  match e with
  | pre_term_bvar i    => pre_term_bvar i
  | pre_term_fvar x    => pre_term_fvar x
  | pre_term_abs V e1  => pre_term_abs  (open_tt_rec K U V)  (open_te_rec K U e1)
  | pre_term_app e1 e2 => pre_term_app  (open_te_rec K U e1) (open_te_rec K U e2)
  | pre_term_tabs e1 => pre_term_tabs (open_te_rec (S K) U e1)
  | pre_term_tapp e1 V => pre_term_tapp (open_te_rec K U e1) (open_tt_rec K U V)
  end.

Definition open_te t U := open_te_rec 0 U t.

Fixpoint open_ee_rec (k : nat) (f : pre_term) (e : pre_term)  : pre_term :=
  match e with
  | pre_term_bvar i    => If k= i then f else (pre_term_bvar i)
  | pre_term_fvar x    => pre_term_fvar x
  | pre_term_abs V e1  => pre_term_abs V (open_ee_rec (S k) f e1)
  | pre_term_app e1 e2 => pre_term_app (open_ee_rec k f e1) (open_ee_rec k f e2)
  | pre_term_tabs e1 => pre_term_tabs (open_ee_rec k f e1)
  | pre_term_tapp e1 V => pre_term_tapp (open_ee_rec k f e1) V
  end.

Definition open_ee t u := open_ee_rec 0 u t.

Notation "T 'open_tt_var' X" := (open_tt T (pre_type_fvar X)) (at level 67).
Notation "t 'open_te_var' X" := (open_te t (pre_type_fvar X)) (at level 67).
Notation "t 'open_ee_var' x" := (open_ee t (pre_term_fvar x)) (at level 67).

Inductive type : pre_type -> Prop :=
  | type_var : forall X,
      type (pre_type_fvar X)
  | type_arrow : forall T1 T2,
      type T1 ->
      type T2 ->
      type (pre_type_arrow T1 T2)
  | type_all : forall L T1,
      (forall X, X \notin L -> type (T1 open_tt_var X)) ->
      type (pre_type_all T1).

Inductive term : pre_term -> Prop :=
  | term_var : forall x,
      term (pre_term_fvar x)
  | term_abs : forall L V e1,
      type V ->
      (forall x, x \notin L -> term (e1 open_ee_var x)) ->
      term (pre_term_abs V e1)
  | term_app : forall e1 e2,
      term e1 ->
      term e2 ->
      term (pre_term_app e1 e2)
  | term_tabs : forall L e1,
      (forall X, X \notin L -> term (e1 open_te_var X)) ->
      term (pre_term_tabs e1)
  | term_tapp : forall e1 V,
      term e1 ->
      type V ->
      term (pre_term_tapp e1 V).


Inductive bind : Set :=
  | bind_type : bind
  | bind_T : pre_type -> bind.

Notation "X ~:*" := (X ~ bind_type)
  (at level 31, left associativity) : env_scope.

Notation "x ~: T" := (x ~ bind_T T)
  (at level 24, left associativity) : env_scope.

Definition ctx := env bind.
(*
Fixpoint dom_t (G:ctx) : list var :=
  match G with
    | nil => nil
    | ((x,T)::rest) => x::(dom_t rest)
  end.
 *)


Inductive wft : ctx -> pre_type -> Prop :=
  | wft_var : forall E X,
      binds X bind_type E ->
      wft E (pre_type_fvar X)
  | wft_arrow : forall E T1 T2,
      wft E T1 ->
      wft E T2 ->
      wft E (pre_type_arrow T1 T2)
  | wft_all : forall L E T,
      (forall X, X \notin L ->
        wft (E & (X ~:*)) (T open_tt_var X)) ->
      wft E (pre_type_all T).

Inductive wfe : ctx -> Prop :=
  | wfe_empty :
      wfe empty
  | wfe_type : forall E X,
      wfe E -> X # E -> wfe (E &(X ~:*))
  | wfe_var : forall E x T,
      wfe E -> wft E T-> x # E -> wfe (E & x ~: T).

(*Inductive wfe : ctx -> Prop :=
  | wfe_empty : wfe nil
  | wfe_ex : forall x T G,  wfe G ->
      ~ (In x (dom_t G)) -> wfe ((x,T)::G).
 *)

Reserved Notation "E |= t ~: T" (at level 69).

Inductive typing : ctx -> pre_term -> pre_type -> Prop :=
  | typing_var : forall E x T,
      wfe E ->
      binds x (bind_T T) E ->
      typing E (pre_term_fvar x) T
  | typing_abs : forall L E V e1 T1,
      (forall x, x \notin L ->
        typing (E & x ~: V) (e1 open_ee_var x) T1) ->
      typing E (pre_term_abs V e1) (pre_type_arrow V T1)
  | typing_app : forall T1 E e1 e2 T2,
      typing E e1 (pre_type_arrow T1 T2) ->
      typing E e2 T1 ->
      typing E (pre_term_app e1 e2) T2
  | typing_tabs : forall L E e1 T1,
      (forall X, X \notin L ->
        typing E (e1 open_te_var X) (T1 open_tt_var X)) ->
      typing E (pre_term_tabs e1) (pre_type_all T1)
  | typing_tapp : forall T1 E e1 T,
      wft E T ->
      typing E e1 (pre_type_all T1) ->
      typing E (pre_term_tapp e1 T) (open_tt T1 T)
where "E |= t ~: T" := (typing E t T).

Inductive value : pre_term -> Prop :=
  | value_abs  : forall V e1, term (pre_term_abs V e1) ->
                 value (pre_term_abs V e1)
  | value_tabs : forall e1, term (pre_term_tabs e1) ->
                 value (pre_term_tabs e1).

Inductive red : pre_term -> pre_term -> Prop :=
  | red_app_1 : forall e1 e1' e2,
      term e2 ->
      red e1 e1' ->
      red (pre_term_app e1 e2) (pre_term_app e1' e2)
  | red_app_2 : forall e1 e2 e2',
      value e1 ->
      red e2 e2' ->
      red (pre_term_app e1 e2) (pre_term_app e1 e2')
  | red_tapp : forall e1 e1' V,
      type V ->
      red e1 e1' ->
      red (pre_term_tapp e1 V) (pre_term_tapp e1' V)
  | red_abs : forall V e1 v2,
      term (pre_term_abs V e1) ->
      value v2 ->
      red (pre_term_app (pre_term_abs V e1) v2) (open_ee e1 v2)
  | red_tabs : forall e1 V2,
      term (pre_term_tabs e1) ->
      type V2 ->
      red (pre_term_tapp (pre_term_tabs e1) V2) (open_te e1 V2).

Notation "t --> t'" := (red t t') (at level 68).