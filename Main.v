Set Implicit Arguments.
Require Import Arith.
Require Import Definitions.
Require Import Substitution.



Inductive neutral : trm -> Prop :=
| var : forall x, neutral(trm_fvar x)
| app : forall e1 e2, neutral(trm_app e1 e2)
| type_app : forall e1 T, neutral(trm_tapp e1 T).



Inductive strong_normalisable : trm -> Prop :=
| val : forall x, term x -> value x  ->
                  strong_normalisable x
| reduct : forall e1 e2, term e1   ->
                         term e2  ->
                         e1 --> e2 ->
                         strong_normalisable e2 ->
                         strong_normalisable e1.


(* term jest normalny jesli zaden jego poterm nie jest postac (\x:U.M)u 
   lub applikacja typu (\T.M)U*)

Inductive normal : trm -> Prop :=
| normal_var : forall x, term (trm_fvar x) ->
                         normal (trm_fvar x)
| normal_abs : forall M T, term (trm_abs T  M) ->
                           normal M ->
                           normal (trm_abs T  M)
| normal_tabs : forall M , term ( trm_tabs M ) ->
                           normal M ->
                           normal (trm_tabs M).

(* t converts to t' t jesli t*)
(*
Definition converts (t : trm) (t' : trm) :=
  exists U M N x,  t = (trm_app (trm_abs U M) N) -> t' = subst_ee x (M open_ee_var x) N
  \/ exists ,  t = (trm_tapp (trm_.
  


Inductive CR1 (t:trm) : Prop :=
  st :  strong_normalisable t -> CR t

with
 CR2 (t:trm)  : Prop :=
  re : exists t',     term t ->
                      term t' ->
                      CR t' ->
                      t' --> t ->
                      CR t
with
   CR (t:trm) : Prop :=
   al : CR1 t /\ CR2 t.
    
  forall t (P : trm->Prop),
                     term t ->
                     P t ->
                     exists t', term t' ->
                     t --> t' ->
                     P t'.
  
Definition CR3 : forall t, t.

Definition CR4 : forall t, t.

*)

  
