# README #

#Wymagania
 * Coq 8.4.6
 * Biblioteka TLC (http://www.chargueraud.org/softs/tlc/) dla Coq 8.4

#Kompilacja
```
#!sh
make target PATHTLC="ścieżka do biblioteki tlc"
```
Domyślny target kompiluje wszystko oprócz pilku Main.v