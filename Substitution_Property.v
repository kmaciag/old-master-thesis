Set Implicit Arguments.
Require Import TLC.LibLN.
Require Import Arith.
Require Import TLC.LibEnv.
Require Import TLC.LibTactics.
Require Import TLC.LibVar.
Require Import Definitions.
Require Import Substitution.
Require Import AutoTactics.

Lemma open_ee_rec_term : forall u e,
  term e -> forall k, e = open_ee_rec k u e.
Proof.
  induction 1; intros; simpl;f_equal*.
    unfold open_ee in *.
    pick_fresh x.
    assert(forall e j v u i, i <> j ->
           open_ee_rec j v e = open_ee_rec i u (open_ee_rec j v e) ->
           e = open_ee_rec i u e).
       induction e; introv Neq P; simpl in *; inversion P; f_equal*;
       case_nat*. case_nat*.
    apply H2 with (j := 0) (v := pre_term_fvar x).
    auto. auto.
    unfold open_te in *.
    pick_fresh X.
    assert(forall e j V u i,
           open_te_rec j V e = open_ee_rec i u (open_te_rec j V e) ->
           e = open_ee_rec i u e).
      induction e; introv P; simpls; inversion P; f_equal*.
    apply H1 with (j := 0) (V := pre_type_fvar X).
    auto.
Qed.

Lemma subst_ee_open_ee : forall t1 t2 u x, term u ->
  subst_ee x u (open_ee t1 t2) =
  open_ee (subst_ee x u t1) (subst_ee x u t2).
Proof.
  intros. unfold open_ee. generalize 0.
  induction t1; intros; simpls; f_equal*.
  case_nat*.
  case_var*.
  rewrite <- open_ee_rec_term.
  auto. auto.
Qed.

Lemma subst_ee_fresh : forall x u e,
  x \notin fv_ee e -> subst_ee x u e = e.
Proof.
  induction e; simpl; intros; f_equal*.
  case_var*.
Qed.

Lemma subst_ee_intro : forall x u e,
  x \notin fv_ee e -> term u ->
  open_ee e u = subst_ee x u (e open_ee_var x).
Proof.
  introv Fr Wu.
  rewrite* subst_ee_open_ee.
  rewrite* subst_ee_fresh.
  simpl. case_var*.
Qed.

Lemma open_tt_rec_type : forall T U,
  type T -> forall k, T = open_tt_rec k U T.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open_tt.
  pick_fresh X.
  assert(forall T j V U i, i <> j ->
         (open_tt_rec j V T) = open_tt_rec i U (open_tt_rec j V T) ->
         T = open_tt_rec i U T).
    induction T; intros; simpl in *; inversion H2; f_equal*.
    case_nat*.
    case_nat*. 
  apply* (@H1 T1 0 (pre_type_fvar X)).
Qed.

Lemma open_te_rec_term_lemma : forall e j u i P ,
  open_ee_rec j u e = open_te_rec i P (open_ee_rec j u e) ->
  e = open_te_rec i P e.
Proof.
  induction e; intros; simpl in *; inversion H; f_equal*; f_equal*.
Qed.

Lemma open_te_rec_type_lemma : forall e j Q i P, i <> j ->
  open_te_rec j Q e = open_te_rec i P (open_te_rec j Q e) ->
   e = open_te_rec i P e.
Proof.
   assert(K :forall T j V U i, i <> j ->
         (open_tt_rec j V T) = open_tt_rec i U (open_tt_rec j V T) ->
         T = open_tt_rec i U T).
    induction T; intros; simpl in *; inversion H0; f_equal*.
    case_nat*.
    case_nat*.
    induction e; intros; simpl in *; inversion H0; f_equal*.
Qed.

Lemma open_te_rec_term : forall e U,
  term e -> forall k, e = open_te_rec k U e.
Proof.
  intros e U WF.
  induction WF; intros; simpl; f_equal*.
  apply* open_tt_rec_type.
  unfolds open_ee. pick_fresh x.
  apply (@open_te_rec_term_lemma e1 0 (pre_term_fvar x)).
  auto.
  unfolds open_te. pick_fresh X.
  apply* (@open_te_rec_type_lemma e1 0 (pre_type_fvar X)).
  pick_fresh x.
  apply* open_tt_rec_type.
Qed.

Lemma subst_ee_open_te_var : forall z u e X, term u ->
  (subst_ee z u e) open_te_var X = subst_ee z u (e open_te_var X).
Proof.
  intros.
  unfold open_te.
   generalize 0.
  induction e; intros; simpl; f_equal*.
  case_var*.
  auto.
  symmetry.  
  apply* open_te_rec_term.
Qed.

Lemma subst_ee_open_ee_var : forall x y u e, y <> x -> term u ->
  (subst_ee x u e) open_ee_var y = subst_ee x u (e open_ee_var y).
Proof.
  intros.
  rewrite* subst_ee_open_ee.
  simpl.
  case_var*.
Qed.

Lemma subst_tt_open_tt_rec : forall T1 T2 X P n, type P ->
  subst_tt X P (open_tt_rec n T2 T1) =
  open_tt_rec n (subst_tt X P T2) (subst_tt X P T1).
Proof.
  intros.
  generalize n.
  induction T1; intros k; simpls; f_equal*.
  case_nat*.
  case_var*.
  rewrite* <- open_tt_rec_type.
Qed.

Lemma subst_tt_open_tt : forall T1 T2 X P, type P ->
  subst_tt X P (open_tt T1 T2) =
  open_tt (subst_tt X P T1) (subst_tt X P T2).
Proof.
  unfold open_tt.
  lets* : subst_tt_open_tt_rec.
Qed.

Lemma subst_te_open_te : forall e T X U, type U ->
  subst_te X U (open_te e T) =
  open_te (subst_te X U e) (subst_tt X U T).
Proof.
  intros.
  unfold open_te.
  generalize 0.
  induction e; intros; simpls; f_equal*;
  lets* : subst_tt_open_tt_rec.
Qed.

Lemma subst_tt_fresh : forall Z U T,
  Z \notin fv_tt T -> subst_tt Z U T = T.
Proof.
  induction T; simpl; intros; f_equal*.
  case_var*.
Qed.

Lemma subst_te_fresh : forall X U e,
  X \notin fv_te e -> subst_te X U e = e.
Proof.
  induction e; simpl; intros; f_equal*;
  lets* : subst_tt_fresh.
Qed.

Lemma subst_te_intro : forall X U e,
  X \notin fv_te e -> type U ->
  open_te e U = subst_te X U (e open_te_var X).
Proof.
  intros.
  rewrite* subst_te_open_te.
  rewrite* subst_te_fresh.
  simpl.
  case_var*.
Qed.

Lemma subst_tt_intro : forall X T2 U,
  X \notin fv_tt T2 -> type U ->
  open_tt T2 U = subst_tt X U (T2 open_tt_var X).
Proof.
  intros.
  rewrite* subst_tt_open_tt.
  rewrite* subst_tt_fresh.
  simpl.
  case_var*.
Qed.

Lemma subst_tt_open_tt_var : forall X Y U T, Y <> X -> type U ->
  (subst_tt X U T) open_tt_var Y = subst_tt X U (T open_tt_var Y).
Proof.
  intros.
  rewrite* subst_tt_open_tt.
  simpl. case_var*.
Qed.

Hint Constructors type term wft ok wfe value red.

Hint Resolve 
  typing_var typing_app typing_tapp.

Lemma subst_tt_type : forall T Z P,
  type T -> type P -> type (subst_tt Z P T).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* type_all as X.
  rewrite* subst_tt_open_tt_var.
Qed.

Lemma subst_te_open_ee_var : forall Z P x e,
  (subst_te Z P e) open_ee_var x = subst_te Z P (e open_ee_var x).
Proof.
  intros.
  unfold open_ee.
  generalize 0.
  induction e; intros; simpl; f_equal*.
  case_nat*.
Qed.

Lemma subst_te_open_te_var : forall X Y U e, Y <> X -> type U ->
  (subst_te X U e) open_te_var Y = subst_te X U (e open_te_var Y).
Proof.
  intros.
  rewrite* subst_te_open_te.
  simpl.
  case_var*.
Qed.

Lemma subst_te_term : forall e Z P,
  term e -> type P -> term (subst_te Z P e).
Proof.
  lets : subst_tt_type.
  induction 1; intros; simpl; auto.
  apply_fresh* term_abs as x.
  rewrite* subst_te_open_ee_var.
  apply_fresh* term_tabs as x.
  rewrite* subst_te_open_te_var.
Qed.

Lemma subst_ee_term : forall e1 Z e2,
  term e1 -> term e2 -> term (subst_ee Z e2 e1).
Proof.
  induction 1; intros; simpl; auto.
  case_var*.
  apply_fresh* term_abs as y.
  rewrite* subst_ee_open_ee_var.
  apply_fresh* term_tabs as Y.
  rewrite* subst_ee_open_te_var.
Qed.

