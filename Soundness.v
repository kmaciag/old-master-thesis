Set Implicit Arguments.
Require Import TLC.LibLN.
Require Import Arith.
Require Import TLC.LibEnv.
Require Import TLC.LibTactics.
Require Import TLC.LibVar.
Require Import TLC.LibLN.
Require Import Definitions.
Require Import Substitution.
Require Import Substitution_Property.
Require Import AutoTactics.


Lemma typing_inv_abs : forall E T1 e1 T,
  typing E (pre_term_abs T1 e1) T ->
  forall T2, T = (pre_type_arrow T1 T2) ->
     exists L, forall x, x \notin L ->
     typing (E &  x ~: T1) (e1 open_ee_var x) T2.
Proof.
  introv Typ.
  gen_eq e:(pre_term_abs T1 e1).
  gen T1 e1.
  induction Typ;
  intros S1 f1 EQ;
  inversions EQ.
  intros T2.
  intros D.
  inversion D.
  let L3 := gather_vars in exists L3.
  intros x a.
  rewrite <- H2.
  apply (@H  x).
  apply notin_union in a.
  destruct a.
  assumption.
Qed.

Lemma ok_from_wfe : forall E,
  wfe E -> ok E.
Proof.
  induction 1; auto.
Qed.

Lemma wfe_push_x_inv : forall E x T,
  wfe (E & x ~: T) -> wfe E /\ wft E T /\ x # E.
Proof.
  intros.
  inversion H.
  apply  empty_push_inv in H1.
  false*.
  lets : (eq_push_inv H0).
  destruct H3 as [?[]].
  subst.
  false*.
  lets : (eq_push_inv H0).
  destruct H4 as [?[]].
  subst.
  split*.
  split*.
  inversion* H5.
  subst*.
Qed.

Lemma wft_weaken : forall G T E F,
  wft (E & G) T ->
  ok (E & F & G) ->
  wft (E & F & G) T.
Proof.
  intros.
  gen_eq K: (E & G).
  gen E F G.
  induction H; intros; subst; auto.
  apply wft_var. 
  apply* binds_weaken.
  apply_fresh* wft_all as Y. 
  apply_ih_bind* H0.
Qed.

Lemma wfe_push_X_inv : forall E X,
  wfe (E &(X~:*)) -> wfe E /\ X # E.
Proof.
  intros.
  inversion H.
  apply empty_push_inv in H1.
  false*.
  lets : (eq_push_inv H0).
  destruct H3 as [?[]].
  subst.
  split*.
  lets : (eq_push_inv H0).
  destruct H4 as [?[]].
  subst.
  split*.
Qed.

Lemma wfe_push_inv : forall E X B,
  wfe (E & X ~ B) -> exists T, B = bind_type \/ B = bind_T T.
Proof.
  intros.
  inversion H.
  apply empty_push_inv in H1.
  false.
  lets : (eq_push_inv H0).
  destruct H3 as [?[]].
  subst.
  exists* (pre_type_fvar X).
  lets : (eq_push_inv H0).
  destruct H4 as [?[]]. 
  subst*.
Qed.

Lemma wfe_weaken : forall E F,
  wfe (E & F) -> wfe E.
Proof.
  induction F using env_ind; rew_env_concat; introv Okt.
  auto.
  lets(T & [H | H]): (wfe_push_inv Okt); subst.
  apply IHF.
  lets*: wfe_push_X_inv Okt.
  apply IHF.
  lets*: wfe_push_x_inv Okt.
Qed.

Hint Extern 1 (ok _) => apply ok_from_wfe.

Hint Resolve subst_tt_type subst_te_term subst_ee_term.

Hint Constructors type term wft ok wfe value red.

Hint Resolve
  typing_var typing_app typing_tapp typing_abs  typing_tabs.

Lemma wft_type : forall E T,
  wft E T -> type T.
Proof.
  induction 1;
  eauto.
Qed.

Lemma wft_from_wfe_var : forall x T E,
  wfe (E & x ~: T) -> wft E T.
Proof.
  intros. inversions* H.
  false (empty_push_inv H1).
  destruct (eq_push_inv H0) as [? [? ?]].
  false.
  destruct (eq_push_inv H0) as [? [? ?]].
  inversion H4. subst*. 
Qed.
  
Lemma wfe_push_x_type : forall E x T,
  wfe (E & x ~: T) -> type T.
Proof.
  intros.
  eapply wft_type.
  forwards*: wfe_push_x_inv.
Qed.

Lemma wft_strengthen : forall E F x U T,
 wft (E & x ~: U & F) T -> wft (E & F) T.
Proof.
  intros.
  gen_eq G: (E & x ~: U & F).
  gen F.
  induction H; intros F EQ; subst; auto.
  apply* wft_var.
  destruct (binds_concat_inv H) as [?|[? ?]].
  apply~ binds_concat_right.
  destruct (binds_push_inv H1) as [[? ?]|[? ?]].
  subst. false.
  apply~ binds_concat_left.
  (*apply IHwft1 with (F0:=F).
  auto.
  apply IHwft2 with (F0:=F).
  auto.*)
  apply_fresh* wft_all as Y.
  apply_ih_bind* H0.
Qed.

Lemma wfe_strengthen : forall x T (E F:ctx),
  wfe (E & x ~: T & F) ->
  wfe (E & F).
Proof.
  introv O. induction F using env_ind.
  rewrite concat_empty_r in *. lets*: (wfe_push_x_inv O).
  rewrite concat_assoc in *.
  lets (U&[?|?]): wfe_push_inv O; subst.
  applys~ wfe_type. apply IHF. applys* wfe_push_X_inv.
  apply ok_from_wfe in O.
  lets (? & H): (ok_push_inv O). eauto.
  applys~ wfe_var. apply IHF. applys* wfe_push_x_inv.
  apply wft_strengthen with (x:=x) (U:=T).
  apply wfe_push_x_inv in O.
  destruct* O.
  apply ok_from_wfe in O.
  lets (? & H): (ok_push_inv O). eauto.
Qed.
Lemma wft_weaken_right : forall T E F,
  wft E T ->
  ok (E & F) ->
  wft (E & F) T.
Proof.
  intros. apply_empty* wft_weaken.
Qed.

Lemma wft_from_env_has_typ : forall x U E,
  wfe E -> binds x (bind_T U) E -> wft E U.
Proof.
  induction E using env_ind; intros Ok B.
  false* binds_empty_inv.
  inversions Ok.
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]].
  subst. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]].
  subst.
  inversions H3.
  assert(H0' := H0).
  apply ok_from_wfe in H0.
  apply_empty* wft_weaken.     
  destruct (eq_push_inv H) as [? [? ?]].
  subst.
  destruct (binds_push_inv B) as [[? ?]|[? ?]].
  subst.
  inversions H4;
  assert(H0' := H0).
  apply ok_from_wfe in H0;
  apply_empty* wft_weaken.
  assert(H0' := H0).
  apply ok_from_wfe in H0.
  apply_empty* wft_weaken.                       
Qed.


Lemma typing_regular : forall E e T,
  typing E e T -> wfe E /\ term e.
Proof.
  induction 1.
  split. auto. apply term_var.
  split.
  pick_fresh y. specializes H0 y. destructs~ H0.
  assert(a : forall x T (E:ctx), wfe (E & x ~: T) -> wfe E).
    introv O. inverts O.
    false* empty_push_inv.
    lets (?&?&?): (eq_push_inv H2). false.
    lets (?&M&?): (eq_push_inv H2). subst. inverts~ M.
    apply* (@a y V E).
    apply_fresh* term_abs as y.
    pick_fresh y. specializes H0 y. apply (@wft_type E).
    apply wft_from_wfe_var with (x:=y).
    destruct H0. apply notin_union in Fr. destruct Fr.
    repeat (apply notin_union in H0;destruct* H0).
    auto.
    specializes H0 y. destructs~ H0.
    splits.  destruct* IHtyping1.
    destruct* IHtyping2.
    splits*.
    pick_fresh y. specializes H0 y. destructs~ H0.
    apply term_tabs with L. intros. apply* H0.
  splits*. lets(H1 & H2): IHtyping.
   apply* term_tapp. apply* wft_type. 
Qed.

Lemma value_regular : forall t,
  value t -> term t.
Proof.
  induction 1; auto.
Qed.

Hint Resolve subst_tt_type subst_te_term subst_ee_term.

Lemma red_regular : forall t t',
  red t t' -> term t /\ term t'.
Proof.
  induction 1; split; try apply* term_app.  
  apply* value_regular. apply* value_regular;
  try apply* term_tapp. apply* term_tapp. apply* term_tapp.
  apply* value_regular.
  inversions H. pick_fresh y. rewrite* (@subst_ee_intro y).
  apply subst_ee_term. apply (@H4 y). apply notin_union in Fr. destruct Fr.
  repeat(apply notin_union in H; destruct H). auto;
  repeat (apply* value_regular). apply* value_regular.
  apply* value_regular.
  apply* term_tapp.
  inversions H. pick_fresh Y. rewrite* (@subst_te_intro Y).
Qed.

Hint Extern 1 (wfe ?E) =>
  match goal with
  | H: typing _ _ _ |- _ => apply (proj31 (typing_regular H))
  end.

Hint Extern 1 (wft ?E ?T) =>
  match goal with
  | H: typing E _ T |- _ => apply (proj33 (typing_regular H))
  end.

Hint Extern 1 (type ?T) =>
  let go E := apply (@wft_type E); auto in
  match goal with
  | H: typing ?E _ T |- _ => go E
  end.

Hint Extern 1 (term ?e) =>
  match goal with
  | H: typing _ ?e _ |- _ => apply (proj32 (typing_regular H))
  | H: red ?e _ |- _ => apply (proj1 (red_regular H))
  | H: red _ ?e |- _ => apply (proj2 (red_regular H))
  end.

Hint Immediate wfe_push_x_type.


Lemma wft_subst_tb : forall F E Z P T,
  wft (E & (Z ~:*) & F) T ->
  wft E P ->
  wfe (E & map (subst_tb Z P) F) ->
  wft (E & map (subst_tb Z P) F) (subst_tt Z P T).
Proof.
 introv WT WP. gen_eq G: (E & (Z~:*) & F). gen F.
  induction WT; intros F EQ Ok; subst; simpl subst_tt; auto.
  case_var*. apply_empty* wft_weaken.
  destruct (binds_concat_inv H) as [S|[Q W]].
  apply* wft_var.
  apply binds_concat_right.
  replace bind_type with ((subst_tb Z P) bind_type) by reflexivity.
  apply* binds_map. 
  apply* wft_var. (* apply binds_concat_right_inv in H.*)     
  apply* binds_concat_left.
  apply binds_concat_left_inv in W.
  auto.
  rewrite dom_single.
  apply* notin_singleton.
  apply_fresh* wft_all as Y.
  unsimpl ((subst_tb Z P) bind_type).
  lets: wft_type.
  rewrite* subst_tt_open_tt_var.
  apply_ih_map_bind* H0.
Qed.

Hint Resolve wft_weaken_right.
Hint Resolve wft_strengthen.
Hint Resolve wft_from_wfe_var.
Hint Immediate wft_from_env_has_typ.
Hint Resolve  wft_weaken wft_type.
Hint Immediate wfe_strengthen.

Lemma typing_weakening : forall E F G e T,
   typing (E & G) e T ->
   wfe (E & F & G) ->
   typing (E & F & G) e T.
Proof.
  introv Typ.
  gen_eq H : (E & G).
  gen G.
  induction Typ; introv EQ Ok; subst.
  apply* typing_var. 
  apply* binds_weaken.
  apply_fresh* typing_abs as x.
  forwards~ K : (H x).
  apply_ih_bind (H0 x);eauto.
  apply typing_app with (T1:=T1).
  auto.
  apply IHTyp2 with (G0 := G).
  auto. auto.
  apply_fresh* typing_tabs as X.  
  apply* typing_tapp.
Qed.

Lemma typing_through_subst_ee : forall U E F x T e u,
  typing (E & x ~: U & F) e T ->
  typing E u U ->
  typing (E & F) (subst_ee x u e) T.
Proof.
  introv TypT TypU. inductions TypT; introv; simpl.
  case_var.
  binds_get H0.
  assert(wfelemma : forall E , wfe E -> ok E).
  induction 1;auto. 
  apply_empty* typing_weakening.
  apply wfe_strengthen in H.
  apply* typing_var. 
  apply binds_subst with  (x1 := x0) (v1 := bind_T T) (x2 := x) (v2 := bind_T U);
  auto.
  apply_fresh* typing_abs as y.
  rewrite* subst_ee_open_ee_var.
  apply_ih_bind* H0.
  lets*: (typing_regular TypU).
  apply* typing_app.
  apply_fresh* typing_tabs as Y.
  rewrite* subst_ee_open_te_var.
  lets*: (typing_regular TypU).
  apply* typing_tapp.
Qed.

Hint Resolve binds_push_eq binds_push_neq
  binds_map binds_concat_left binds_concat_right.

Lemma wfe_subst_tb : forall Z P E F,
  wfe (E & (Z~:*) & F) ->
  wft E P ->
  wfe (E & map (subst_tb Z P) F).
Proof.
  induction F using env_ind. simpl; intros Ok WP.
  eauto.
  rewrite -> map_empty with (f:=subst_tb Z P).
  rewrite concat_empty_r in Ok.
  rewrite concat_empty_r.
  apply wfe_push_X_inv in Ok.
  destruct* Ok.
  intros OK H.
  repeat (rewrite concat_assoc in *).
(*  rewrite map_push.
  simpl subst_tb.*)
  lets (U&[?|?]): wfe_push_inv OK. subst.
  lets*: (wfe_push_X_inv OK).
  destruct H0.
  forwards* L :(IHF H0).
  apply wfe_type  with (X:=x) in L.
  auto.
  repeat( rewrite concat_assoc in *).
  rewrite map_push.
  simpl  subst_tb. auto.
  rewrite concat_assoc. auto.
  lets*: (wfe_push_X_inv OK).
  subst.
  rewrite map_push.
  simpl subst_tb. auto.
    lets*:(wfe_push_x_inv OK).
  simpl subst_tb.
  destruct H0.
  destruct H1.
  forwards*L : (IHF H0).
  apply wfe_var with (x:=x) (T:=subst_tt Z P U) in L.
  rewrite concat_assoc.
  auto. apply* wft_subst_tb. 
  lets*:(wfe_push_x_inv OK).
Qed.

Lemma notin_fv_tt_open : forall Y X T,
  X \notin fv_tt (T open_tt_var Y) ->
  X \notin fv_tt T.
Proof.
 introv. unfold open_tt. generalize 0.
 induction T; simpl; intros k Fr; auto.
 lets :  (@IHT1 k).
 lets : (@IHT2 k).
 auto.
 apply* IHT.
Qed.
          
Lemma map_subst_tb_id : forall G Z P,
  wfe G -> Z # G -> G = map (subst_tb Z P) G.
Proof.
  induction 1; simpl; intros Fr; auto.
  rewrite -> map_empty with (f:=subst_tb Z P). auto.
  simpl Fr;
  rewrite -> dom_concat in Fr;
  apply notin_union in Fr; destruct Fr;  
  forwards L :(IHwfe H1);
  rewrite -> L at 1;
  rewrite* -> map_push;
  simpl; auto.
  rewrite -> dom_concat in Fr.
  apply notin_union in Fr.
  destruct Fr.
  forwards L:(IHwfe H2).
  rewrite -> L at 1.
  rewrite -> map_push.
  simpl. 
  auto.  
  rewrite* subst_tt_fresh.
  assert(forall E X T, wft E T -> X # E -> X \notin fv_tt T).
      induction 1; intros Fr; simpl.
      eauto. 
      rewrite notin_singleton.
      intro.
      rewrite -> H5 in Fr.
      apply(binds_fresh_inv H4 Fr).
      apply notin_union.
      split*.
      pick_fresh Q.
      apply (@notin_fv_tt_open Q).
      apply (@H5 Q).
      apply notin_union in Fr0.
      destruct Fr0.
      repeat(apply notin_union in H6 ;destruct H6).
      auto.
      rewrite dom_concat.
      apply notin_union.
      split*.
  apply (@H4 E Z T); auto.
Qed.

Lemma typing_through_subst_te : forall  E F Z e T P,
  typing (E & (Z~:*) & F) e T ->
  wft E P ->
  typing (E & (map (subst_tb Z P) F)) (subst_te Z P e) (subst_tt Z P T).
Proof.
introv Typ Wft. gen_eq G : (E & (Z~:*) & F). gen F.
induction Typ; introv EQ; subst.
apply* typing_var.
apply* wfe_subst_tb.
rewrite* (@map_subst_tb_id E Z P).
binds_cases H0.
unsimpl ((subst_tb Z P) (bind_T  T)).
rewrite <- map_concat.
apply binds_map.
apply* binds_concat_left.
rewrite <-map_concat.
unsimpl ((subst_tb Z P) (bind_T  T)).
apply binds_map.
apply* binds_concat_right.
apply wfe_weaken in H.
apply wfe_weaken in H.
auto.
simpls subst_te.
simpls subst_tb.
simpls subst_tt.
apply_fresh* typing_abs as y.
unsimpl (subst_tb Z P (bind_T V)).
rewrite* subst_te_open_ee_var.
(*
apply notin_union in Fry.
destruct Fry.
 repeat(apply notin_union in H1 ;destruct H1).
apply (@H0 y H1 F ).*)
(* do porawy automatyzacja*)
apply_ih_map_bind* H0;
rewrite concat_assoc;
rewrite concat_assoc.
auto.
apply* typing_app.
simpls subst_te.
simpls subst_tb.
simpls subst_tt.
apply_fresh* typing_tabs as Y.
unsimpl (subst_tb Z P (bind_type)).
rewrite* subst_te_open_te_var.
rewrite* subst_tt_open_tt_var.
rewrite* subst_tt_open_tt.
simpls subst_tb.
simpls subst_tt.
apply* typing_tapp. 
apply* wft_subst_tb. auto.
apply* wfe_subst_tb.
Qed.

Theorem preservation : forall E t t' T,
  typing E t T ->
  t --> t' ->
  typing E t' T.
Proof.
  introv Typ.
  gen t'.
  induction Typ;
  introv Red;
  try solve [inversion Red].
  inversions Red; try solve [ apply* typing_app ].
  inversions Typ1. pick_fresh x. forwards~ K: (H2 x).
  rewrite* (@subst_ee_intro x).
  apply_empty typing_through_subst_ee; substs*.
  lets*: typing_regular Typ2.
  inversions Red. try solve [apply * typing_tapp].
  inversion Typ.
  pick_fresh y. forwards~ K: (H5 y).
  rewrite* (@subst_te_intro y).
  rewrite* (@subst_tt_intro y).
  assert(E =  E & map (subst_tb y T) empty).
    rewrite map_empty.
    rewrite*  concat_empty_r.
  rewrite -> H6.
  apply* typing_through_subst_te.
  rewrite* concat_empty_r. substs.
  apply_empty* typing_weakening.
Qed.

Lemma canonical_form_abs : forall e U1 U2,
  value e ->
  typing empty e (pre_type_arrow U1 U2) ->
  exists V, exists e1, e = pre_term_abs V e1.
Proof.
  intros.
  remember empty as E.
  remember (pre_type_arrow U1 U2) as T.
  revert U1 U2 HeqT HeqE.
  induction H0.
  inversion H.
  intros.
  subst.
  eauto.
  intros.
  inversion H.
  intros.
  subst*.
  eauto.
  inversion HeqT.
  intros.
  inversion H.
Qed.

Lemma canonical_form_tabs : forall t U1, 
  value t -> typing empty t (pre_type_all U1) -> 
  exists e1, t = pre_term_tabs e1. 
Proof.
  intros.
  remember empty as E.
  remember (pre_type_all U1) as T.
  revert U1  HeqT HeqT.
  induction H0.
  inversion H.
  intros.
  subst.
  eauto.
  intros.
  inversion H.
  intros.
  subst*.
  eauto.
  inversion HeqT.
  intros.
  inversion H.
  intros.
  eauto.
  intros.
  inversion H.
Qed.

(* Progress *)

Theorem progress : forall e T,
  typing empty e T ->
  value e \/ exists e', red e e'.
Proof with eauto.
  intros e T Typ.
  remember empty as E.
  generalize dependent HeqE.
  assert( T' : typing E e T)...
  induction Typ; intros EQ; subst.  
  lets : binds_empty_inv.
  false*.
  left.
    apply value_abs.
    lets :  typing_regular.
    lets K:(@H1 empty (pre_term_abs V e1) (pre_type_arrow V T1)).
    lets J:(@K T').
    destruct* J.
  right.
    destruct* IHTyp1 as [?|[? ?]]. 
    destruct* IHTyp2 as [?|[? ?]]. 
    destruct (canonical_form_abs H Typ1) as [S [e3 EQ]]. 
    subst.
    exists* (open_ee e3 e2).
    subst. 
    apply* red_abs.
    lets*: typing_regular Typ1.  
    exists* (pre_term_app x e2).
    apply* red_app_1.
    lets*: typing_regular Typ2. 
  left. 
    apply value_tabs.
    lets* : typing_regular T'.
  right.
    destruct~ IHTyp as [?| [? ?]]. 
    destruct (canonical_form_tabs H0 Typ) as [e EQ]. 
    subst.
    exists* (open_te e T).
    apply* red_tabs.
    lets*: typing_regular Typ. 
    autos* wft_type. 
Qed.