PATHTLC?="./tlc/src"

all: Definitions Substitution AutoTactics Substitution_Property Soundness

Definitions: Definitions.v
				coqc -R $(PATHTLC) "TLC" Definitions.v

Substitution: Definitions Substitution.v
				coqc -R $(PATHTLC) "TLC" Substitution.v
 
AutoTactics: Definitions Substitution AutoTactics.v 
				coqc -R $(PATHTLC) "TLC" AutoTactics.v 

Substitution_Property: Substitution_Property.v
				coqc -R $(PATHTLC) "TLC" Substitution_Property.v

Soundness: Substitution_Property Soundness.v
				coqc -R $(PATHTLC) "TLC" Soundness.v	

Main: Main.v
				coqc -R $(PATHTLC) "TLC" Main.v				

.PHONY: clean

clean::
				rm -f *~
				rm -f *.vio *.v.d *.vo *.vq *.vk *.aux .*.aux *.glob
				rm -rf .coq-native .coqide