Set Implicit Arguments.
Require Import TLC.LibLN.
Require Import Arith.
Require Import TLC.LibEnv.
Require Import TLC.LibTactics.
Require Import TLC.LibVar.
Require Import TLC.LibLN.
Require Import Definitions.

Fixpoint fv_tt (T : pre_type) : fset(var) :=
  match T with
  | pre_type_bvar J      => \{}
  | pre_type_fvar X      => \{X}
  | pre_type_arrow T1 T2 => (fv_tt T1) \u (fv_tt T2)
  | pre_type_all T1   => (fv_tt T1)
  end.

Fixpoint fv_te (e : pre_term) : fset(var) :=
  match e with
  | pre_term_bvar i    => \{}
  | pre_term_fvar x    => \{x}
  | pre_term_abs V e1  => (fv_tt V) \u (fv_te e1)
  | pre_term_app e1 e2 => (fv_te e1) \u (fv_te e2)
  | pre_term_tabs e1 => (fv_te e1)
  | pre_term_tapp e1 V => (fv_tt V) \u (fv_te e1)
  end.

Fixpoint fv_ee (e : pre_term) : vars :=
  match e with
  | pre_term_bvar i    => \{}
  | pre_term_fvar x    => \{x}
  | pre_term_abs V e1  => (fv_ee e1)
  | pre_term_app e1 e2 => (fv_ee e1) \u (fv_ee e2)
  | pre_term_tabs e1 => (fv_ee e1)
  | pre_term_tapp e1 V => (fv_ee e1)
  end.

Fixpoint subst_tt (Z : var) (U : pre_type) (T : pre_type) : pre_type :=
  match T with
  | pre_type_bvar J      => pre_type_bvar J
  | pre_type_fvar X      => If X = Z then U else (pre_type_fvar X)
  | pre_type_arrow T1 T2 => pre_type_arrow (subst_tt Z U T1) (subst_tt Z U T2)
  | pre_type_all T1  => pre_type_all (subst_tt Z U T1)
  end.

Fixpoint subst_te (Z : var) (U : pre_type) (e : pre_term) : pre_term :=
  match e with
  | pre_term_bvar i    => pre_term_bvar i
  | pre_term_fvar x    => pre_term_fvar x
  | pre_term_abs V e1  => pre_term_abs  (subst_tt Z U V)  (subst_te Z U e1)
  | pre_term_app e1 e2 => pre_term_app  (subst_te Z U e1) (subst_te Z U e2)
  | pre_term_tabs  e1  => pre_term_tabs (subst_te Z U e1)
  | pre_term_tapp e1 V => pre_term_tapp (subst_te Z U e1) (subst_tt Z U V)
  end.

Fixpoint subst_ee (z : var) (u : pre_term) (e : pre_term)  : pre_term :=
  match e with
  | pre_term_bvar i    => pre_term_bvar i
  | pre_term_fvar x    => If x = z then u else (pre_term_fvar x)
  | pre_term_abs V e1  => pre_term_abs V (subst_ee z u e1)
  | pre_term_app e1 e2 => pre_term_app (subst_ee z u e1) (subst_ee z u e2)
  | pre_term_tabs e1 => pre_term_tabs (subst_ee z u e1)
  | pre_term_tapp e1 V => pre_term_tapp (subst_ee z u e1) V
  end.

Definition subst_tb (Z : var) (P : pre_type) (b : bind) : bind :=
  match b with
  | bind_type  => bind_type
  | bind_T T => bind_T (subst_tt Z P T)
  end.