
Require Import TLC.LibLN.
Require Import Arith.
Require Import Definitions.
Require Import Substitution.

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x }) in
  let C := gather_vars_with (fun x : pre_term => fv_te x) in
  let D := gather_vars_with (fun x : pre_term => fv_ee x) in
  let E := gather_vars_with (fun x : pre_type => fv_tt x) in
  let F := gather_vars_with (fun x : ctx => dom x) in
  constr:(A \u B \u C \u D \u E \u F).

Ltac pick_fresh x :=
  let L := gather_vars in (pick_fresh_gen L x).

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

Ltac get_env :=
  match goal with
  | |- wft ?E _ => E
  | |- typing ?E _ _ => E
  end.

Tactic Notation "apply_empty_bis" tactic(get_env) constr(lemma) :=
  let E := get_env in rewrite <- (concat_empty_r E);
  eapply lemma; try rewrite concat_empty_r.

Tactic Notation "apply_empty" constr(F) :=
  apply_empty_bis (get_env) F.

Tactic Notation "apply_empty" "*" constr(F) :=
  apply_empty F; autos*.